/*
 * Server certificate variables
*/
variable "certificate_chain" {
  type          = string
  description   = "Certificate chain of server certificate"
}

variable "server_private_key" {
  type          = string
  description   = "Server private key"
}

variable "server_certificate_body" {
  type          = string
  description   = "Server certificate body"
}

variable "server_cert_tag" {
  type          = string
  description   = "A tag for the server certificate tag"
  default       = "VPN server certificate"
}

variable "vpn_cidr_block" {
  type          = string
  description   = "VPN CIDR block"
}

variable "subnets" {
  type          = list(string)
  description   = "A list of subnets to associate with the Amazon Client VPN endpoint"
}

variable "vpc_cidr_block" {
  type          = string
  description   = "The target network CIDR"
}

variable "vpn_client_vpn_endpoint_tag" {
  type          = string
  description   = "A tag for the VPN Client VPN Endpoint"
  default       = "Client VPN Endpoint"
}

/*
 * Security group variables
*/
variable "vpn_security_group_name" {
  type          = string
  description   = "Name of the security group for the Client VPN Endpoint"
  default       = "security group for vpn traffic"
}

variable "vpc_id" {
  type          = string
  description   = "VPC ID"
}


variable "security_group_id" {
  type          = string
  description   = "ID of the security group to allow traffic from VPN"
}

variable "rds_security_group_id" {
  type          = string
  description   = "ID of the RDS security group to allow traffic from VPN"
}

# Detail: https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html
variable "dns_servers" {
  type          = list(string)
  description   = "DNS server(s) used to resolve DNS. We can use Amazon DNS server which is the base of the VPC IPv4 network range plus two"
}
